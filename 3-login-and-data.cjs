/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)



Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/


function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
}

const { error, log } = require('console');
/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/

/*
Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)
*/

const fs = require('fs');
const path = require('path');

let filePath1 = path.resolve(__dirname, './file1.txt');
let filePath2 = path.resolve(__dirname, './file2.txt');
let data = "new file created";

let createFile1 = new Promise((resolve, reject) => {
    fs.writeFile(filePath1, data, (error) => {
        if (error) {
            reject(error);
        } else {
            resolve();
        }
    });
});

let createFile2 = new Promise((resolve, reject) => {
    fs.writeFile(filePath2, data, (error) => {
        if (error) {
            reject(error);
        }
        else {
            resolve();
        }
    });
});

Promise.all([createFile1, createFile2])
    .then(() => {
        console.log("files created");
    })
    .catch(()=>{
        console.error("error while creating file");
    });

function deleteFile(filePath) {
    return new Promise((resolve, reject) => {
        fs.unlink(filePath, (error) => {
            if (error) {
                reject(error);
            }
            else {
                resolve();
            }
        });
    });
}

setTimeout(() => {
    deleteFile(filePath1)
        .then(() => {
            deleteFile(filePath2);
        }).then(() => {
            console.log('All files deleted');
        }).catch((err) => {
            console.error("error while deleting file");
        });
}, 2000);
